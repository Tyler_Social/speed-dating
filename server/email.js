require('dotenv').config();

const crypto = require('crypto');
const sgMail = require('@sendgrid/mail');

const Event = require('./models/event');
const RateEventToken = require('./models/rateEventToken');
const { handleError, handleEventErrors } = require('./utils');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendRatingEmail = eventId => {
  Event.findById(eventId, handleEventErrors(null, null, event => {
    const emails = [];
    Object.keys(event.guests).forEach(key => {
      const guest = event.guests[key];
      RateEventToken.create({
        eventId,
        email: guest.email,
        token: crypto.randomBytes(16).toString('hex'),
      }, handleError(null, token => {
        emails.push({
          to: guest.email,
          from: process.env.SENDER,
          subject: `Rate ${event.name}, so we can send you your matches' profiles`,
          text: `
            Hi ${guest.nickname},\n
            Please, access ${process.env.CLIENT_HOST + '/?token=' + token.token} to rate ${event.name}, in order to get your matches' profiles.\n
            Thank you!
          `,
          html: `
            <p>Hi ${guest.nickname},</p>
            <p>Please, <a href="${process.env.CLIENT_HOST + '/?token=' + token.token}">rate ${event.name}</a>, in order to get your matches' profiles.</p>
            <p>Thank you!</p>
          `
        });

        if ( emails.length === Object.keys(event.guests).length ) {
          sgMail.send(emails)
            .then(arg => {
              // console.log(arg);
            })
            .catch(err => {
              console.log(err);
            });
        }
      }));
    });
  }));
}

const sendMatchesProfilesEmail = (eventId, guestEmail) => {
  Event.findById(eventId, handleEventErrors(null, null, event => {
    const matches = [];
    const guest = event.guests[guestEmail];

    const email = {
      from: process.env.SENDER,
      subject: `These are your matches' profiles from ${event.name}`,
      to: guestEmail,
      text: `Hi ${guest.firstName} ${guest.lastName},
        These are your matches' profiles:`,
      html: `
        <p>Hi ${guest.firstName} ${guest.lastName},</p>
        <p>These are your matches' profiles:</p>
        <ul>
      `,
    };

    Object.keys(event.guests).forEach(matchEmail => {
      const match = event.guests[matchEmail];
      const matchMatched = match.matches && match.matches[guest.code] && match.matches[guest.code] !== 'false';
      const guestMatched = guest.matches && guest.matches[match.code] && guest.matches[match.code] !== 'false';

      if (
        ( event.secondChance && matchMatched ) ||
        ( !event.secondChance && matchMatched && guestMatched )
      ) {
        matches.push(match);
      }
    });

    matches.forEach(match => {
      email.text += `${match.firstName} ${match.lastName}: ${match.email}\n`;
      email.html += `<li>${match.firstName} ${match.lastName}: ${match.email}</li>`;
    });

    email.html += '</ul>';

    sgMail.send(email)
      .then(arg => {
        // console.log(arg);
      })
      .catch(err => {
        console.log(err);
      });
  }));
}

const sendHostCode = eventId => {
  Event.findById(eventId, handleEventErrors(null, null, event => {
    const email = {
      from: process.env.SENDER,
      subject: `IMPORTANT - Your Access Code to ${event.name} on Intro-Social.com`,
      to: event.host.email,
      text: `Hi ${event.host.firstName} ${event.host.lastName}, Thank you for setting up your new event with Intro-Social!

      Keep this code handy. Bookmark / star / save this email. You'll need it to log-in to your event and control the action!

      EVENT CODE: ${event.code} , SECRET HOST CODE: ${event.hostCode}

      You will give-out the event code to all guests when they arrive. They will use this code to log-in to the live event.

      Use the "secret host code" in exhange for your email address in the live event to access the "host panel". You will need to log-in this way to start the event.

      Feel free to visit <a href="https://live.intro-social.com">https://live.intro-social.com</a> to test your event before it happens!

      If you have any questions, feel free to reach out at <a href="www.intro-social.com">www.intro-social.com</a>.`,
      html: `<p>Hi ${event.host.firstName} ${event.host.lastName}, Thank you for setting up your new event with Intro-Social!</p>
      <p>Keep this code handy. Bookmark / star / save this email. You'll need it to log-in to your event and control the action!</p>
      <p style="font-size: 2em; font-weight: 700;">EVENT CODE: ${event.code} , SECRET HOST CODE: ${event.hostCode}</p>
      <p>You will give-out the event code to all guests when they arrive. They will use this code to log-in to the live event.</p>
      <p>Use the "secret host code" in exhange for your email address in the live event to access the "host panel". You will need to log-in this way to start the event.</p>
      <p>Feel free to visit <a href="https://live.intro-social.com">https://live.intro-social.com</a> to test your event before it happens!</p>
      <p>If you have any questions, feel free to reach out at <a href="www.intro-social.com">www.intro-social.com</a>.</p>`,
    };

    sgMail.send(email)
      .then(arg => {
        // console.log(arg);
      })
      .catch(err => {
        console.log(err);
      });
  }));
}

module.exports = {
  sendHostCode,
  sendMatchesProfilesEmail,
  sendRatingEmail,
}
