const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();
const _ = require('lodash');

const { handleError, handleEventErrors } = require('../utils');
const {
  addSocket,
  pauseEvent,
  removeSocket,
  resumeEvent,
  skipCurrentRound,
  skipCurrentStep,
  startEvent,
} = require('../eventManager');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

const Event = require('../models/event');

const checkHostCode = (req, res, next) => {
  const eventCode = req.query.eventCode || req.body.eventCode || null;
  let hostCode = req.query.hostCode || req.body.hostCode || null;

  if ( !eventCode ) {
    res.status(400).send({ message: 'No event code provided.' });
    return;
  }

  Event.findOne({ code: eventCode }, handleEventErrors(res, eventCode, event => {
    if ( /^\d{1,6}$/.test(hostCode) ) {
      hostCode = parseInt(hostCode, 10);
    }

    if ( event.hostCode !== hostCode ) {
      res.status(403).send({ message: 'Invalid host code.' });
      return;
    }

    next();
  }));
}

router.post('/nextStep', checkHostCode, function (req, res) {
  const { eventCode } = req.body;

  Event.findOne({ code: eventCode }, handleEventErrors(res, eventCode, event => {
    skipCurrentStep(event._id);
    res.status(200).send({ message: 'Success!' });
  }));
});

router.post('/nextRound', checkHostCode, function (req, res) {
  const { eventCode } = req.body;

  Event.findOne({ code: eventCode }, handleEventErrors(res, eventCode, event => {
    skipCurrentRound(event._id);
    res.status(200).send({ message: 'Success!' });
  }));
});

router.post('/startEvent', checkHostCode, function (req, res) {
  const { eventCode } = req.body;

  Event.findOne({ code: eventCode }, handleEventErrors(res, eventCode, event => {
    startEvent(event._id);
    res.status(200).send({ message: 'Success!' });
  }));
});

router.post('/pauseEvent', checkHostCode, function (req, res) {
  const { eventCode } = req.body;

  Event.findOne({ code: eventCode }, handleEventErrors(res, eventCode, event => {
    pauseEvent(event._id);
    res.status(200).send({ message: 'Success!' });
  }));
});

router.post('/resumeEvent', checkHostCode, function (req, res) {
  const { eventCode } = req.body;

  Event.findOne({ code: eventCode }, handleEventErrors(res, eventCode, event => {
    resumeEvent(event._id);
    res.status(200).send({ message: 'Success!' });
  }));
});

router.post('/updateStepsDuration', checkHostCode, function (req, res) {
  const { eventCode, steps } = req.body;

  Event.findOne({ code: eventCode }, handleEventErrors(res, eventCode, event => {
    const stepsDuration = event.stepsDuration;

    Object.keys(stepsDuration).forEach(stepKey => {
      stepsDuration[stepKey] = steps[stepKey];
    });

    Event.findByIdAndUpdate(event._id, {
      $set: {
        stepsDuration,
      }
    }, handleError(res, () => {
      res.status(200).send({ message: 'Steps\' duration updated.' })
    }));
  }));
});

module.exports = router;
