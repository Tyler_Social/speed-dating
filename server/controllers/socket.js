const moment = require('moment');
const _ = require('lodash');

const { cancelEventStart } = require('../agenda');
const {
  addSocket,
  getAdjustedEndDate,
  getCurrentStep,
  hasStarted,
  moveToWaitingChannel,
  removeSocket,
} = require('../eventManager');
const Event = require('../models/event');
const { handleError, handleEventErrors } = require('../utils');
const { getGuestData } = require('../wp');

const waitingDuration = 300;
// const waitingDuration = 30;

const getGuest = email => (new Promise((resolve, reject) => {
  getGuestData(email).then(guest => {
    if ( guest === null ) {
      reject(Error('Guest not found: ' + email + '. Please create a profile first.'));
    }

    resolve(guest);
  });
}));

const updateMatching = (eventId, guest, operation) =>  (new Promise((resolve, reject) => {
  Event.findById(eventId, handleEventErrors(null, null, event => {
    const matching = event.matching;

    if ( operation === 'add' ) {
      if ( matching.participants.includes(guest.email)) {
        reject(Error('Guest: ' + guest.email + ', already connected.'));
      }

      matching.participants = _.union(matching.participants, [guest.email]);
    } else {
      matching.participants = _.without(matching.participants, guest.email);
    }

    Event.findByIdAndUpdate(event._id, {
      $set: {
        matching,
      }
    }, handleError());

    resolve(matching);
  }));
}));

const updateGuestsAndGetGuest = (event, email, socket) => (new Promise((resolve, reject) => {
  getGuest(email).then(guest => {
    const guests = event.guests;
    if ( !guests[email] ) {
      let guestCodes = event.guestCodes;
      guest.code = guestCodes[0];
      guestCodes = guestCodes.slice(1);
      guests[email] = guest;

      resolve(guest);

      Event.findByIdAndUpdate(event._id, {
        $set: {
          guests,
          guestCodes,
        }
      }, handleError());
    } else {
      resolve(guests[email]);

      guest = guests[email];
    }

    socket.to('host').emit('eventData', {
      guests: _.values(guests),
    });

    socket.emit('eventData', {
      guestCode: guest.code,
    });
  }).catch(e => {
    reject(e);
  });
}));

const addGuestAndGetMatching = (event, email, socket) => (new Promise((resolve, reject) => {
  updateGuestsAndGetGuest(event, email, socket).then(guest => {
    updateMatching(event._id, guest, 'add').then(matching => {
      resolve(matching);
    });
  }).catch(e => {
    reject(e);
  });
}));

const removeGuest = (event, email) => {
  const guest = event.guests[email];

  if ( guest ) {
    updateMatching(event, guest, 'remove');
  }
}

const onConnection = socket => {
  const nspName = socket.nsp.name.replace('/', '');

  Event.findOne({ code: nspName }, handleEventErrors(null, nspName, event => {

    let email = socket.handshake.query.email;

    if ( /^\d{1,6}$/.test(email) ) {
      email = parseInt(email, 10);
    }

    if ( email === event.hostCode ) {
      socket.nsp.in('host').clients((err, clients) => {
        if ( err ) {
          console.log(err);
          socket.emit('message', e.message);
          socket.disconnect();
          return;
        }

        if ( clients.length > 0 ) {
          const message = 'Host already connected';
          console.log(message);
          socket.emit('message', message);
          socket.disconnect();
        } else {
          socket.join('host');
          socket.emit('eventData', {
            eventName: event.name,
            guests: _.values(event.guests),
            steps: event.stepsDuration
          });

          const currentStep = getCurrentStep(event._id, socket);
          socket.emit('changeStep', currentStep);
          socket.emit('changePage', 'host');
        }
      });
    } else {
      const oneHour = 60 * 60 * 1000;
      const now = (new Date()).getTime();

      if ( event.startDate.getTime() - oneHour > now ) {
        socket.emit('message', 'Event isn\'t open yet. Event starts at ' + moment(event.startDate).format('h:mm a') + '.');
        socket.disconnect(true);
        return;
      } else if ( getAdjustedEndDate(event) < now ) {
        socket.emit('message', 'Event has already ended.');
        socket.disconnect(true);
        return;
      }

      addGuestAndGetMatching(event, email, socket).then(matching => {
        addSocket(event._id, email, socket);
        socket.emit('eventData', { eventName: event.name });

        const started = hasStarted(event._id);
        if ( started ) {
          const currentStep = getCurrentStep(event._id, socket);
          socket.emit('changeStep', currentStep);
          moveToWaitingChannel(email, event._id);
        } else {
          socket.emit('changeStep', 'waiting');
          socket.emit('changePage', 'waiting');
          socket.emit('timer', waitingDuration);
        }
      }).catch(e => {
        socket.emit('message', e.message);
        socket.disconnect(true);
      });

      socket.on('disconnect', onDisconnect(event.code, email));
    }
  }, socket));
}

const onDisconnect = (eventCode, email) => socket => {
  Event.findOne({ code: eventCode }, handleEventErrors(null, eventCode, event => {
    removeSocket(event._id, email);
    removeGuest(event, email);
  }, socket));
}

const onSkipRound = socket => {
  Event.findOne({ code: socket.nsp.name.replace('/', '') }, handleError(null, event => {
    moveToWaitingChannel(socket.email, event._id);
  }));
}

const createSocket = io => {
  const eventsNsp = io.of(/^\/\d{1,4}/);

  eventsNsp.on('connection', onConnection);
  eventsNsp.on('skipRound', onSkipRound);
}

module.exports = {
  createSocket,
};
