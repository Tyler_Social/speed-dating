const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();
const _ = require('lodash');

const { handleError, handleEventErrors } = require('../utils');
const { sendMatchesProfilesEmail } = require('../email');
const { moveToWaitingChannel } = require('../eventManager');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

const Event = require('../models/event');
const RateEventToken = require('../models/rateEventToken');

router.get('/nextMatch', function (req, res) {
  const { email, eventCode} = req.query;

  Event.findOne({ code: eventCode }, handleEventErrors( res, eventCode, event => {
    const matches = event.matching.matches[event.matching.currentRound];

    if ( matches[email] ) {
      const matchEmail = matches[email].match;
      const match = event.guests[matchEmail];
      res.status(200).send({
        code: match.code,
        description: match.description,
        profilePath: match.profilePath,
      });
    } else {
      moveToWaitingChannel(email, event._id);
      res.status(200).send({
        code: -1,
      });
    }
  }));
});

router.get('/rateMatch', function (req, res) {
  const { email, match, matchCode, eventCode } = req.query;

  Event.findOne({ code: eventCode }, handleEventErrors( res, eventCode, event => {
    const guest = event.guests[email];
    const matches = event.matching.matches[event.matching.currentRound];

    if ( match !== 'null' ) {
      matches[email].meet = true;
    }

    if ( !guest.matches ) {
      guest.matches = {
        [matchCode]: match === 'true',
      };
    } else {
      guest.matches[matchCode] = match === 'true';
    }

    Event.findByIdAndUpdate(event._id, { $set: {
      guests: event.guests,
      matching: event.matching,
    } }, handleError(res, () => {
      res.status(200).send({ message: 'Wait for next round to start.' });
    }));
  }));
});

router.get('/rateEvent/:token', function(req, res) {
  RateEventToken.findOne({ token: req.params.token }, handleError(res, rateEventToken => {
    if ( rateEventToken ) {
      const { eventId, email } = rateEventToken;
      Event.findById(eventId, handleEventErrors(res, null, event => {
        if ( event ) {
          let ratings
          if ( event.ratings ) {
            ratings = event.ratings;

            if ( !ratings[email] ) {
              ratings[email] = {
                comment: req.query.comment,
                rating: req.query.rating,
              };
            } else {
              res.status(200).send({ message: 'You have already rated this event.' });
              return;
            }
          } else {
            ratings = {
              [email]: {
                comment: req.query.comment,
                rating: req.query.rating,
              },
            };
          }

          Event.findByIdAndUpdate(eventId, { $set: { ratings } }, handleEventErrors(res, null, () => {
            sendMatchesProfilesEmail(eventId, email);
            res.status(200).send({ message: 'You\'ll receive an email with your matches\' profiles.' });
          }));
        }
      }));
    }
  }));
});

module.exports = router;
