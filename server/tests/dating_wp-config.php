<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tythesci_WPYNL');

/** MySQL database username */
define('DB_USER', 'tythesci_WPYNL');

/** MySQL database password */
define('DB_PASSWORD', 'ezdO$9%%NJel610*z');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '05d5ae712f1c296ba9a38c0b1be889005fabea8e652782e769c3e8b1d6d71c04');
define('SECURE_AUTH_KEY', '7daf81c62a5cc919cc8fd44a61f7123efb78745ffa98317c66173d2b02fb8b81');
define('LOGGED_IN_KEY', 'be426c0788dc486a8afca1d1434a32b5ec294b10affd86346b2baa4eaf20cd4a');
define('NONCE_KEY', 'f059ef65438647d55be96aa45d6bd1cc502d14b8452cf942c6b7d600741fd16c');
define('AUTH_SALT', '984ff60a85760a6b312ed52c4f661c7faf9d96a82c6367341a15e19a8f5893f3');
define('SECURE_AUTH_SALT', 'e511abc275d18be8b06b465769fc7688a6c0e92990bcbf3c4dcd739cc960acff');
define('LOGGED_IN_SALT', '6c30d8d985784acebf81558904fe4f80547c9707b687b184c201305818f49523');
define('NONCE_SALT', '310a9b1bd4adb35707e2b9a372502f5e80b2da707b2e97aa01b872a9e7144f73');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'YBr_';
define('WP_CRON_LOCK_TIMEOUT', 120);
define('AUTOSAVE_INTERVAL', 300);
define('WP_POST_REVISIONS', 5);
define('EMPTY_TRASH_DAYS', 7);
define('WP_AUTO_UPDATE_CORE', true);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
