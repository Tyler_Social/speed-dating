<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tythesci_WPGZJ');

/** MySQL database username */
define('DB_USER', 'tythesci_WPGZJ');

/** MySQL database password */
define('DB_PASSWORD', 'IL.tq0aPL00fUL?F<');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '5a4b65c8b0a900c75ef93b3d01bc5d8f373bb6c8eb84550d6d4d872e697f82aa');
define('SECURE_AUTH_KEY', 'b1a5376879e6f686867e89dcf4d019089d078fb4a26c43c7001d82038c105049');
define('LOGGED_IN_KEY', '53394a14b3f000c93022661fafcf2b1a5457f73e40bce0b7d7149ba226ef98bd');
define('NONCE_KEY', '9717f374c350cd9c46a03f3de96431210757c45c0afe9d3b1edb2ab5820bec8c');
define('AUTH_SALT', 'f34c3288ebbbaba30ddae3e083ca807958dea09f6b857ce40a0d4fefbe42d460');
define('SECURE_AUTH_SALT', 'b68b2df1a2057c5f11e7caa9a03a986054b0b512d663da36f705bc15d468081d');
define('LOGGED_IN_SALT', 'c8052a56be27c0520aa16488e67f21cbbd49e135008958bbd76bc0db83d636f0');
define('NONCE_SALT', '9b2a57e78d5543e4a12dd03efe455e6c1f4880caf26cdad16135b1e8f27da69d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'Oj5_';
define('WP_CRON_LOCK_TIMEOUT', 120);
define('AUTOSAVE_INTERVAL', 300);
define('WP_POST_REVISIONS', 5);
define('EMPTY_TRASH_DAYS', 7);
define('WP_AUTO_UPDATE_CORE', true);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
