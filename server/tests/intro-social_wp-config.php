<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tythesci_WPVOU');

/** MySQL database username */
define('DB_USER', 'tythesci_WPVOU');

/** MySQL database password */
define('DB_PASSWORD', 't(iXE&)w2jV^8}mp[');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'c10e51056f22563aa004ddbccc0a51855fdd3cccc3bab0b7462afb578e8b7c96');
define('SECURE_AUTH_KEY', '37d19dd677acf080925183eef40dd7c7fece15657d2770c1c231241d3ba23ff3');
define('LOGGED_IN_KEY', 'f30d8da769e71da7feb3859d960a3e6c79e98d4dfddd48146896cada00c2ac65');
define('NONCE_KEY', 'a5c736b2493dcf44ebb1b4bf7ad3a8f5b5526279bea7e635bc21b13d5aa8c0a0');
define('AUTH_SALT', '5dc707cb22dbffdb481ba2e81030a03cb27e063b84abe80f8ba3202a2853ded8');
define('SECURE_AUTH_SALT', 'ed9bc4426c7c19523bc73b109f060ae3e56482c535184af620c4342a2a6abd9b');
define('LOGGED_IN_SALT', '005bbca7d108dc65c775e9b4f246b9ed554b92d0ce9fdccf7033830a6cfdd0a7');
define('NONCE_SALT', '41ee69ffba7b83ab0ef71cf2afdc4dc894f3b5e1d14d97bc83aa01c01d593220');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'Lg6_';
define('WP_CRON_LOCK_TIMEOUT', 120);
define('AUTOSAVE_INTERVAL', 300);
define('WP_POST_REVISIONS', 5);
define('EMPTY_TRASH_DAYS', 7);
define('WP_AUTO_UPDATE_CORE', true);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
