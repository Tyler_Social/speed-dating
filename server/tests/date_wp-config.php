<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tythesci_WPRV7');

/** MySQL database username */
define('DB_USER', 'tythesci_WPRV7');

/** MySQL database password */
define('DB_PASSWORD', 'F>&*ElQ-2Q*Tu_RvT');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '5ac16ae8334adde04d923eafc58760300cb79655091a4fcbf4e25be35328c6e8');
define('SECURE_AUTH_KEY', '96555fa66a98ca70d6f797eadd6fec01cdec643c4399bbc3815509d4dfe3a85d');
define('LOGGED_IN_KEY', '3e5caddbd8f9dafa49b9f72397609414a57ed423b9aadbb06d1cfc9bd1a473b8');
define('NONCE_KEY', 'b0174ddce16a1de656a6d2d0ca7ad57ec9847a0a44449a387289a1d03e9d2799');
define('AUTH_SALT', '70cb32d37adb015102025158099dd791f9279233066c9b0092938033b878d34d');
define('SECURE_AUTH_SALT', 'febf3c6d279a2a1c62874da55ad4e83087ad84803281782f97359cb2f82c8a13');
define('LOGGED_IN_SALT', 'b56dacae3af5ed298957daddcf3de122c7e2b038b205c7eab8693c6d1ec4ad15');
define('NONCE_SALT', 'eee5469aff175434cfb39761375832d100de9d98091f9e65a65ddbf694a00457');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'Pez_';
define('WP_CRON_LOCK_TIMEOUT', 120);
define('AUTOSAVE_INTERVAL', 300);
define('WP_POST_REVISIONS', 5);
define('EMPTY_TRASH_DAYS', 7);
define('WP_AUTO_UPDATE_CORE', true);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
