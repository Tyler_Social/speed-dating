<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tythesci_WPTL9');

/** MySQL database username */
define('DB_USER', 'tythesci_WPTL9');

/** MySQL database password */
define('DB_PASSWORD', 'zZ?/FBN7EpRbO9J^9');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '8915a912a6f7b4983898956d069a2231a969be9995b16cda71a517070ed88cda');
define('SECURE_AUTH_KEY', '21b5a72aa9993ea17f50b26cf29b3b87ef2c2193a9c627bd4b02b6ff7f545ff8');
define('LOGGED_IN_KEY', 'fdcbf59ae882b187f03a169ad85e0d3ed834b2aecff9e7d995be9ab0fa408eae');
define('NONCE_KEY', 'c47346885318df4b1f2e58b3c81a3d6be93a609942f713dc4d93fead0ef63eca');
define('AUTH_SALT', '7414bafd3e9c0a9591dab4b3aaa6446167066f0e4f9c3938c6aa05f313a9e6c9');
define('SECURE_AUTH_SALT', 'bfa5d71a4bfa750e85a8e34b679bf3ee7e13a4955ffe02546afbfc6237e763a0');
define('LOGGED_IN_SALT', '9d196023147b5b25ce79ea86a1bac4ff45c07a82323d8c9a25da3272f80f28e7');
define('NONCE_SALT', 'da5a32f4a124ec2c0d038840906dffca244ae523f75637ce86e21d8b1917195f');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'c8L_';
define('WP_CRON_LOCK_TIMEOUT', 120);
define('AUTOSAVE_INTERVAL', 300);
define('WP_POST_REVISIONS', 5);
define('EMPTY_TRASH_DAYS', 7);
define('WP_AUTO_UPDATE_CORE', true);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
