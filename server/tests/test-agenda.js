const db = require('../db');
db.connect();

const agenda = require('../agenda');

(async function() {
  await agenda.start();
  await agenda.schedule('in 1 minute', 'sendRatingEmail', { eventId: '5f529ca8cd745ed2a6806b8c' });
})();
