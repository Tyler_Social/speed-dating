const db = require('./db');

db.connect();

const Room = require('./models/room');

const r = new Room({
    date: new Date(2020, 7, 7),
    startTime: new Date(2020, 7, 7, 19),
    endTime: new Date(2020, 7, 7, 22),
});

Room.create(r, function(err, room) {
    if (err) {
        console.log(err);
        return;
    }

    console.log(room);
});
