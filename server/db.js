require('dotenv').config();

const mongoose = require('mongoose');

// mongoose.set('debug', true);

mongoose.connection.on('connected', () => {
  console.log('Mongoose default connection is open.');
});

mongoose.connection.on('error', err => {
  console.log('Mongoose default connection has got an error');
  console.log(err);
});

mongoose.connection.on('disconnected', () => {
  console.log('Mongoose default connection is disconnected');
});

process.on('SIGINT', () => {
  mongoose.connection.close(() =>{
    console.log('Mongoose default connection is disconnected due to application termination');
    process.exit(0);
  });
});

const getConnectionURL = () => {
  const userPass = '';
  if (process.env.DB_USER && process.env.DB_PASS) {
    userPass = process.env.DB_USER + ':' + process.env.DB_PASS + '@';
  }

  if (!process.env.MONGO_HOST) {
    throw new Error('No MONGO_HOST defined');
  }

  if (!process.env.MONGO_DB) {
    throw new Error('No MONGO_DB defined');
  }

  return `mongodb://${userPass}${process.env.MONGO_HOST}/${process.env.MONGO_DB}`;
}

const connect = () => {
  mongoose.Promise = Promise;

  return mongoose.connect(getConnectionURL(), {
    autoIndex: false,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }).catch(reason => {
    console.log('Unable to connect to the mongodb instance. Error: ', reason);
  });
}

module.exports = {
  mongoose,
  connect,
  disconnect: (done) => {
    mongoose.disconnect(done);
  },
  getConnectionURL,
};
