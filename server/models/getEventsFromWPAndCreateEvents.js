const moment = require('moment');
const _ = require('lodash');

const Event = require('./event');

const db = require('../db');
db.connect();

// Simulate an event found
const wpEvent = {
  eventName: 'J\'s Awesome Dating Event',
  eventStartDateUTC: '2020-09-02 19:45:00',
  eventEndDateUTC: '2020-09-02 20:45:00',
  meetType: 'Heterosexual Dating (default)',
  // matchRoundDuration: '5',
  // chatRoundDuration: '10',
  matchRoundDuration: '0.5',
  chatRoundDuration: '0.5',
  matchMode: 'Second Chance Mode',
  guests: [
    {
      nickname: 'Joao\'s test user',
      HowILookToday: 'My description so I can be recognized in a crowd',
      ProfilePhoto: 'https://date.intro-social.com/wp-content/uploads/wpforms/17-5b6b8c391866e871821c418f5a3ba8a9/joaoneto-ae9f0414308237cd3942bf3f509396f4.jpg',
      DateofBirth: '12/13/1987',
      Gender: 'Male', // Male; Female; Third Gender / Non-binary / Fluid; Other / Prefer not to answer;
      MatchGender: 'Male', // Male; Female; Non-binary / Fluid; Third Gender / Trans / Other;
      ContactMethod: 'Email',
      user_email: 'joao.neto+1@toptal.com',
    },
    {
      nickname: 'Joao\'s second test user',
      HowILookToday: 'My description so I can be recognized in a crowd',
      ProfilePhoto: 'https://date.intro-social.com/wp-content/uploads/wpforms/17-5b6b8c391866e871821c418f5a3ba8a9/joaoneto-ae9f0414308237cd3942bf3f509396f4.jpg',
      DateofBirth: '12/13/1987',
      Gender: 'Male', // Male; Female; Third Gender / Non-binary / Fluid; Other / Prefer not to answer;
      MatchGender: 'Female', // Male; Female; Non-binary / Fluid; Third Gender / Trans / Other;
      ContactMethod: 'Email',
      user_email: 'joao.neto+2@toptal.com',
    },
    {
      nickname: 'Joao\' third test user',
      HowILookToday: 'My description so I can be recognized in a crowd',
      ProfilePhoto: 'https://date.intro-social.com/wp-content/uploads/wpforms/17-5b6b8c391866e871821c418f5a3ba8a9/joaoneto-ae9f0414308237cd3942bf3f509396f4.jpg',
      DateofBirth: '12/13/1987',
      Gender: 'Female', // Male; Female; Third Gender / Non-binary / Fluid; Other / Prefer not to answer;
      MatchGender: 'Male', // Male; Female; Non-binary / Fluid; Third Gender / Trans / Other;
      ContactMethod: 'Email',
      user_email: 'joao.neto+3@toptal.com',
    },
    {
      nickname: 'Joao\' fourth test user',
      HowILookToday: 'My description so I can be recognized in a crowd',
      ProfilePhoto: 'https://date.intro-social.com/wp-content/uploads/wpforms/17-5b6b8c391866e871821c418f5a3ba8a9/joaoneto-ae9f0414308237cd3942bf3f509396f4.jpg',
      DateofBirth: '12/13/1987',
      Gender: 'Female', // Male; Female; Third Gender / Non-binary / Fluid; Other / Prefer not to answer;
      MatchGender: 'Male', // Male; Female; Non-binary / Fluid; Third Gender / Trans / Other;
      ContactMethod: 'Email',
      user_email: 'joao.neto+4@toptal.com',
    }
  ]
};

const guestCodesAmount = 1000;

const getEventsFromWPAndCreateEvents = function() {
  const event = new Event({
    eventName: wpEvent.eventName,
    eventStartDateUTC: moment.utc(wpEvent.eventStartDateUTC).toDate(),
    eventEndDateUTC: moment.utc(wpEvent.eventEndDateUTC).toDate(),
    meetType: wpEvent.meetType.replace(' (default)', ''),
    stepsDuration: {
      matching: wpEvent.matchRoundDuration * 60,
      chat: wpEvent.chatRoundDuration * 60,
      rateMatch: 30, // the default is 1
    },
    matchMode: wpEvent.matchMode.replace(' Mode', ''),
    guests: {},
    guestCodes: _.shuffle(Array(guestCodesAmount).fill(0).map((v, i) => i.toString())),
  });

  wpEvent.guests.forEach(guest => {
    const dateOfBirthArray = guest.DateofBirth.split('/');

    const newGuest = {
      nickname: guest.nickname,
      description: guest.HowILookToday,
      profilePath: guest.ProfilePhoto,
      dateOfBirth: new Date(dateOfBirthArray[2], parseInt(dateOfBirthArray[0], 10) - 1, dateOfBirthArray[1]),
      gender: guest.Gender,
      matchGender: guest.MatchGender.split('\n'),
      code: event.guestCodes[0],
      email: guest.user_email,
    };

    event.guestCodes = event.guestCodes.slice(1);
    event.guests[newGuest.code] = newGuest;
  });

  event.save(function(err) {
    if (err) {
      console.log(err);
    } else {
      console.log('finished!!');
    }

    db.disconnect();
  });
}

getEventsFromWPAndCreateEvents();

// module.exports = getEventsFromWPAndCreateEvents
