const _ = require('lodash');
const mongoose = require('mongoose');

const RateEventTokenSchema = new mongoose.Schema({
  eventId: {
    ref: 'Event',
    required: true,
    type: mongoose.Schema.Types.ObjectId
  },
  email: {
    required: true,
    type: String,
  },
  token: {
    required: true,
    type: String
  },
  createdAt: {
    default: Date.now,
    expires: 259200, // 3 days
    required: true,
    type: Date
  }
});

mongoose.model('RateEventToken', RateEventTokenSchema);

module.exports = mongoose.model('RateEventToken');
