const _ = require('lodash');
const mongoose = require('mongoose');

const { getNextAvailableCode } = require('./code');

const EventSchema = new mongoose.Schema({
  mysqlId: {
    required: true,
    type: String,
    unique: true,
  },
  name: {
    required: true,
    type: String,
  },
  startDate: { // UTC
    required: true,
    type: Date,
  },
  endDate: { // UTC
    required: true,
    type: Date,
  },
  meetType: {
    default: 'Heterosexual Dating',
    enum: [
      'Heterosexual Dating',
      'Homosexual Dating',
      'Non-Binary / Bisexual / Pansexual Dating',
    ],
    required: true,
    type: String,
  },
  stepsDuration: {
    matching: {
      default: 5 * 60,
      required: true,
      type: Number,
    },
    chat: {
      default: 10 * 60,
      required: true,
      type: Number,
    },
    rateMatch: {
      default: 1 * 60,
      required: true,
      type: Number,
    },
  },
  secondChance: {
    required: true,
    type: Boolean,
  },
  capacity: {
    default: 0,
    type: Number,
  },
  guests: {
    default: {},
    type: mongoose.Schema.Types.Mixed,
    /*
      [email]: {
        nickname,
        description,
        profilePath,
        dateOfBirth,
        gender, // ['Male', 'Female', 'Third Gender / Non-binary / Fluid', 'Other / Prefer not to answer']
        matchGender, // ['Male', 'Female', 'Non-binary / Fluid', 'Third Gender / Trans / Other']
        code,
        matches, // { matchCode: Boolean }
        email,
        contactMethod,
        id,
      }
    */
  },
  code: {
    required: true,
    type: String,
    unique: true,
  },
  guestCodes: {
    required: true,
    type: Array,
    of: Number,
  },
  matching: {
    default: {
      currentRound: 0,
      participants: [], // [ email, ... ]
      matches: [], // [ { email: { match: email, meet: Boolean }, email: { match: email, meet: Boolean }, ... }, ... ]
    },
    type: mongoose.Schema.Types.Mixed
  },
  ratings: {
    default: {},
    type: mongoose.Schema.Types.Mixed, // { email: rating, .... }
  },
  hostCode: {
    required: true,
    type: Number,
  },
  host: {
    default: {},
    type: mongoose.Schema.Types.Mixed, // { email, firstName, lastName }
  }
});

EventSchema.pre('validate', function() {
  const self = this;
  return new Promise(async function(resolve, reject) {
    if (self.isNew) {
      try {
        self.code = await getNextAvailableCode();
        resolve();
      } catch(e) {
        console.log(e);
        reject();
      }
    }
  });
});

mongoose.model('Event', EventSchema);

module.exports = mongoose.model('Event');
