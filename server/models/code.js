const mongoose = require('mongoose');
const _ = require('lodash');

const codesAmount = 10000;

const CodeSchema = new mongoose.Schema({
    codes: [ Number ]
});

const Code = mongoose.model('Code', CodeSchema);

Code.countDocuments({}, function(err, count) {
    if (err) {
        console.log(err);
        reject(new Error('Error finding codes.'));
        return;
    }

    if (count === 0) {
        const codes = _.shuffle(Array(codesAmount).fill(0).map((v, i) => i));
        Code.create({ codes }, function(err, codeList) {
            if (err) {
                console.log(err);
                reject(new Error('Error creating codes.'));
                return;
            }
        });
    }
});

const getNextAvailableCode = () => {
    return new Promise((resolve, reject) => {
        Code.findOne({}, (err, codesList) => {
            if (err) {
                console.log(err);
                reject(new Error('Error finding codes.'));
                return;
            }

            if (!codesList || !codesList.codes || codesList.codes.length === 0) {
                reject(new Error('There is no available code to use.'));
                return;
            }

            resolve(codesList.codes[0]);

            codesList.codes = codesList.codes.slice(1);
            codesList.save();
        });
    });
}

const returnCodeToCodeList = code => {
    Code.findOne({}, (err, codesList) => {
        if (err) {
            console.log('Error finding codes.');
            console.log(err);
            return;
        }

        if (!codesList || !codesList.codes) {
            console.log('There is no code list.');
            return;
        }

        codesList.codes.push(code);
        codesList.save();
    });
}

module.exports = {
    getNextAvailableCode,
    returnCodeToCodeList,
};
