const express = require('express');

const { startGettingEvents } = require('./agenda')
const eventController = require('./controllers/event');
const hostController = require('./controllers/host');

const app = express();

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, x-access-token');
  next();
});

app.use('/api/event', eventController);
app.use('/api/host', hostController);

startGettingEvents();

module.exports = app;
