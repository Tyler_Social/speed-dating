const handleError = ( res, callback, socket ) => ( err, result ) => {
  if ( err ) {
    console.log(err);

    if ( res ) {
      res.status(500).send({ message: err.message });
    }

    if ( socket ) {
      socket.disconnect(true);
    }

    return;
  }

  if ( callback ) {
    callback(result);
  }
}

const handleEventErrors = ( res, eventCode, callback, socket ) => {
  return handleError(res, event => {
    if ( !event ) {
      const message = 'Event not found: ' + eventCode;
      console.log(message);

      if ( res ) {
        res.status(404).send({ message });
      }

      if ( socket ) {
        socket.emit('message', message);
      }

      return;
    }

    if ( callback ) {
      callback(event);
    }
  }, socket)
}

module.exports = {
  handleError,
  handleEventErrors,
};
