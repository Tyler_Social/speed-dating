require('dotenv').config();

const moment = require('moment');
const mysql = require('mysql');
const _ = require('lodash');

['MYSQL_HOST', 'MYSQL_USER', 'MYSQL_DB', 'MYSQL_PASS' ].forEach(envVar => {
  if ( !process.env[envVar] ) {
    throw new Error('No ' + envVar + ' defined');
  }
});

const mysqlMetaKeysToGuestKeys = {
  nickname: 'nickname',
  first_name: 'firstName',
  last_name: 'lastName',
  _DOB: 'dateOfBirth',
  _Gender: 'gender',
  _ContactMethod: 'contactMethod',
  _HowILook: 'description',
  _MatchGender: 'matchGender',
  jrL_user_avatar: 'profilePath',
};

// const guestsMockData = {
//   'joao.neto+m1@toptal.com': {
//     id: 1,
//     email: 'joao.neto+m1@toptal.com',
//     lastName: 'Neto',
//     nickname: 'j-male-1',
//     firstName: 'Joao',
//     dateOfBirth: '12/13/1987',
//     gender: 'Male',
//     contactMethod: 'Email',
//     description: 'Male, the first one.',
//     matchGender: 'Male\nFemale'
//   },
//   'joao.neto+m2@toptal.com': {
//     id: 2,
//     email: 'joao.neto+m2@toptal.com',
//     nickname: 'j-male-2',
//     firstName: 'Joao',
//     lastName: 'Neto',
//     dateOfBirth: '12/13/1987',
//     gender: 'Male',
//     contactMethod: 'Email',
//     description: "I'm a male, the second one.",
//     matchGender: 'Male\nFemale'
//   },
//   'joao.neto+m3@toptal.com': {
//     id: 3,
//     email: 'joao.neto+m3@toptal.com',
//     nickname: 'j-male-3',
//     firstName: 'Joao',
//     lastName: 'Neto',
//     dateOfBirth: '12/13/1987',
//     gender: 'Male',
//     contactMethod: 'Email',
//     description: "I'm a male, the third one.",
//     matchGender: 'Male\nFemale'
//   },
//   'joao.neto+m4@toptal.com': {
//     id: 4,
//     email: 'joao.neto+m4@toptal.com',
//     nickname: 'j-male-4',
//     firstName: 'Joao',
//     lastName: 'Neto',
//     dateOfBirth: '12/13/1987',
//     gender: 'Male',
//     contactMethod: 'Email',
//     description: "I'm a male, the fourth one.",
//     matchGender: 'Male\nFemale'
//   },
//   'joao.neto+f1@toptal.com': {
//     id: 5,
//     email: 'joao.neto+f1@toptal.com',
//     nickname: 'j-female-1',
//     firstName: 'Joao',
//     lastName: 'Neto',
//     dateOfBirth: '12/13/1987',
//     gender: 'Female',
//     contactMethod: 'Email',
//     description: 'Female test user, #1.',
//     matchGender: 'Male\nFemale'
//   },
//   'joao.neto+f2@toptal.com': {
//     id: 6,
//     email: 'joao.neto+f2@toptal.com',
//     matchGender: 'Male\nFemale',
//     description: 'Female, the second one.',
//     contactMethod: 'Email',
//     gender: 'Female',
//     dateOfBirth: '12/13/1987',
//     firstName: 'Joao',
//     nickname: 'j-female-2',
//     lastName: 'Neto'
//   },
//   'joao.neto+f3@toptal.com': {
//     id: 7,
//     email: 'joao.neto+f3@toptal.com',
//     matchGender: 'Male\nFemale',
//     description: 'Female, the third one.',
//     contactMethod: 'Email',
//     gender: 'Female',
//     dateOfBirth: '12/13/1987',
//     firstName: 'Joao',
//     nickname: 'j-female-3',
//     lastName: 'Neto'
//   },
//   'joao.neto+f4@toptal.com': {
//     id: 8,
//     email: 'joao.neto+f4@toptal.com',
//     matchGender: 'Male\nFemale',
//     description: 'Female, the second one.',
//     contactMethod: 'Email',
//     gender: 'Female',
//     dateOfBirth: '12/13/1987',
//     firstName: 'Joao',
//     nickname: 'j-female-4',
//     lastName: 'Neto'
//   },
//   'joao.neto+o1@toptal.com': {
//     id: 9,
//     email: 'joao.neto+o1@toptal.com',
//     matchGender: 'Male\nFemale\nNon-binary / Fluid\nThird Gender / Trans / Other',
//     description: 'Other, the first one.',
//     contactMethod: 'Email',
//     gender: 'Third Gender / Non-binary / Fluid',
//     dateOfBirth: '12/13/1987',
//     firstName: 'Joao',
//     nickname: 'j-other-1',
//     lastName: 'Neto'
//   },
//   'joao.neto+o2@toptal.com': {
//     id: 10,
//     email: 'joao.neto+o2@toptal.com',
//     matchGender: 'Male\nFemale\nNon-binary / Fluid\nThird Gender / Trans / Other',
//     description: 'Other, the second one.',
//     contactMethod: 'Email',
//     gender: 'Other / Prefer not to answer',
//     dateOfBirth: '12/13/1987',
//     firstName: 'Joao',
//     nickname: 'j-other-2',
//     lastName: 'Neto'
//   },
// };

// const getGuestData = email => (new Promise((resolve, reject) => {
//   const guest = guestsMockData[email];
//   if ( guest ) {
//     const guestClone = _.cloneDeep(guest);
//     guestClone.matchGender = guestClone.matchGender.split('\n');
//     resolve(guestClone);
//   } else {
//     resolve(null);
//   }
// }));

const getGuestData = email => (new Promise((resolve, reject) => {
  const connection = mysql.createConnection({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASS,
    database: process.env.MYSQL_DB,
  });

  connection.connect();

  connection.query(`
    SELECT m.meta_key, m.meta_value, u.ID FROM jrL_users AS u JOIN jrL_usermeta AS m ON u.ID = m.user_id
    WHERE m.meta_key IN ("${Object.keys(mysqlMetaKeysToGuestKeys).join('","')}")
    AND u.user_email="${email}";
  `, (error, results, fields) => {
    if ( error ) {
      reject(error);
      connection.end();
      return;
    }

    if ( results.length > 0 ) {
      const guestData = { email };
      results.forEach(result => {
        if ( !guestData.id ) {
          guestData.id = result.ID;
        }

        if ( mysqlMetaKeysToGuestKeys[result.meta_key] === 'matchGender' ) {
          guestData[mysqlMetaKeysToGuestKeys[result.meta_key]] = result.meta_value.split('\n');
        } else {
          guestData[mysqlMetaKeysToGuestKeys[result.meta_key]] = result.meta_value;
        }
      });

      if ( guestData.profilePath ) {
        connection.query(`
          SELECT guid FROM jrL_posts
          WHERE ID=${guestData.profilePath};
        `, (error, results, fields) => {
          if ( error ) {
            reject(error);
            connection.end();
            return;
          }

          if ( results.length === 1 ) {
            guestData.profilePath = results[0].guid;
          } else {
            guestData.profilePath = '';
          }

          resolve(guestData);
          connection.end();
        });
      } else {
        resolve(guestData);
        connection.end();
      }
    } else {
      resolve(null);
      connection.end();
    }
  });
}));

const mysqlMetaKeysToEventKeys = {
  _EventStartDateUTC: 'startDate',
  _EventEndDateUTC: 'endDate',
  _tribe_ticket_capacity: 'capacity',
  _ecp_custom_6: 'secondChance',
  _ecp_custom_7: 'matching',
  _ecp_custom_8: 'chat',
  _ecp_custom_9: 'rateMatch',
  _ecp_custom_32: 'meetType',
  first_name: 'firstName',
  last_name: 'lastName',
};

// const eventsMockData = {
//   '1027': {
//     name: "J's event",
//     host: {
//       id: 26,
//       email: 'joao.neto+host@toptal.com',
//       firstName: 'Joao',
//       lastName: 'Host'
//     },
//     capacity: '2',
//     startDate: new Date('2020-11-17T19:00:00.000Z'),
//     endDate: new Date('2020-11-17T19:15:00.000Z'),
//     secondChance: true,
//     matching: '1 minutes',
//     chat: '1 minutes',
//     rateMatch: '1 minutes',
//     meetType: 'Heterosexual Dating',
//     // meetType: 'Homosexual Dating',
//     // meetType: 'Non-Binary / Bisexual / Pansexual Dating',
//     mysqlId: '1600',
//   },
// };

// const getEventsData = () => (new Promise((resolve, reject) => {
//   resolve(_.cloneDeep(eventsMockData));
// }));

const getEventsData = () => (new Promise((resolve, reject) => {
  const connection = mysql.createConnection({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASS,
    database: process.env.MYSQL_DB,
  });

  connection.connect();

  connection.query(`
    SELECT p.ID, p.post_title, p.post_author, m.meta_key, m.meta_value FROM jrL_posts AS p
      JOIN jrL_postmeta AS m ON p.ID = m.post_id
      WHERE m.meta_key IN ("${Object.keys(mysqlMetaKeysToEventKeys).join('","')}")
      AND post_status="publish" AND post_type="tribe_events";
  `, (error, results, fields) => {
    if ( error ) {
      reject(error);
      connection.end();
      return;
    }

    if ( results.length > 0 ) {
      const eventsData = {};
      results.forEach(result => {
        if ( result.meta_key === '_EventStartDateUTC' || result.meta_key === '_EventEndDateUTC' ) {
          result.meta_value = moment.utc(result.meta_value).toDate();
        }

        // set flag to filter events that already ended
        const now = new Date();
        if ( result.meta_key === '_EventEndDateUTC' && result.meta_value < now ) {
          eventsData[result.ID] = { pastEvent: true };
        }

        if ( result.meta_key === '_ecp_custom_6' ) {
          result.meta_value = result.meta_value === '' ? false : true;
        }

        if ( !eventsData[result.ID] ) {
          eventsData[result.ID] = {
            name: result.post_title,
            host: { id: result.post_author },
            [mysqlMetaKeysToEventKeys[result.meta_key]]: result.meta_value,
          };
        } else {
          eventsData[result.ID][mysqlMetaKeysToEventKeys[result.meta_key]] = result.meta_value;
        }
      });

      const filteredEventsData = {};
      Object.keys(eventsData).forEach(mysqlId => {
        if (!eventsData[mysqlId].pastEvent) {
          filteredEventsData[mysqlId] = eventsData[mysqlId];
        }
      });

      if (Object.keys(filteredEventsData).length > 0) {
        let queryCounter = 0;
        Object.keys(filteredEventsData).forEach(eventMysqlId => {
          const host = filteredEventsData[eventMysqlId].host;
          connection.query(`
            SELECT u.user_email, m.meta_key, m.meta_value
              FROM jrL_users AS u
              JOIN jrL_usermeta AS m ON u.ID = m.user_id
              WHERE m.meta_key IN ("first_name","last_name")
              AND u.ID = ${host.id};
          `, (error, results, fields) => {
            queryCounter += 1;
            if ( error ) {
              reject(error);
              connection.end();
              return;
            }

            if ( results.length > 0 ) {
              results.forEach(result => {
                host.email = result.user_email;
                host[mysqlMetaKeysToEventKeys[result.meta_key]] = result.meta_value;
              });
            }

            if ( queryCounter === Object.keys(filteredEventsData).length ) {
              let capacityQueryCounter = 0;
              Object.keys(filteredEventsData).forEach(eventMysqlId => {
                connection.query(`
                  SELECT m1.meta_value
                  FROM jrL_postmeta AS m1 JOIN jrL_postmeta AS m2 ON m1.post_id = m2.post_id
                  WHERE m2.meta_value=${eventMysqlId}
                  AND m1.meta_key=\"_tribe_ticket_capacity\"
                  AND m2.meta_key=\"_tribe_wooticket_for_event\";
                `, (error, results, fields) => {
                  capacityQueryCounter += 1;
                  if ( error ) {
                    reject(error);
                    connection.end();
                    return;
                  }

                  if ( results.length > 0 ) {
                    let capacitySum = 0;
                    results.forEach(result => {
                      capacitySum += parseInt(result.meta_value, 10);
                    });
                    filteredEventsData[eventMysqlId].capacity = capacitySum;
                  }

                  if ( capacityQueryCounter === Object.keys(filteredEventsData).length ) {
                    resolve(filteredEventsData);
                    connection.end();
                  }
                });
              });
            }
          });
        });
      } else {
        resolve(null);
        connection.end();
      }
    } else {
      resolve(null);
      connection.end();
    }
  });
}));

module.exports = {
  getEventsData,
  getGuestData,
}
