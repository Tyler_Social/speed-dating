const _ = require('lodash');
const { handleError, handleEventErrors } = require('./utils');
const Event = require('./models/event');
const { runMatching } = require('./matching/matching');

const waitingDuration = 300;
// const waitingDuration = 5;

const eventsData = {};

const createEventData = eventId => {
  eventsData[eventId] = {
    currentStep: 'waiting',
    interval: null,
    nsp: null,
    remainingTime: waitingDuration,
    sockets: {},
    started: false,
    waiting: [],
  }
}

const getEventData = eventId => {
  if ( !eventsData[eventId] ) {
    createEventData(eventId);
  }

  return eventsData[eventId];
};

const getAdjustedEndDate = event => (
  event.endDate.getTime() - (1000 * (
    event.stepsDuration.matching +
    event.stepsDuration.chat +
    event.stepsDuration.rateMatch
  ))
)

const clearEventInterval = eventId => {
  const eventData = getEventData(eventId);
  clearInterval(eventData.interval);
  eventData.interval = null;
}

const countDown = (eventId, time, handleTimeChange) => {
  return new Promise((resolve, reject) => {
    const eventData = getEventData(eventId);
    const startTime = (new Date()).getTime();

    handleTimeChange(time);

    eventData.interval = setInterval(() => {
      eventData.remainingTime = time - Math.floor(((new Date()).getTime() - startTime) / 1000);

      if (eventData.remainingTime >= 0) {
        handleTimeChange(eventData.remainingTime);
      } else {
        clearEventInterval(eventId);
        resolve();
      }
    }, 1000);
  });
}

const handleTimeChange = (eventId, step) => remainingTime => {
  const eventData = getEventData(eventId);
  eventData.nsp && eventData.nsp.to('playing').to('waiting').to('host').emit('timer', remainingTime);
}

const hasStarted = eventId => {
  const eventData = getEventData(eventId);
  return eventData.started;
}

const startEvent = eventId => {
  const eventData = getEventData(eventId);
  if ( !eventData.started ) {
    eventData.started = true;

    eventData.nsp && eventData.nsp.emit('changeStep', 'waiting');
    countDown(eventId, waitingDuration, handleTimeChange(eventId, 'waiting')).then(() => {
      startRound(eventId);
    });
  }
}

const nextStep = {
  waiting: 'startRound',
  matching: 'chat',
  chat: 'rateMatch',
  rateMatch: null,
};

const startRound = eventId => {
  removeAllFromWaitingChannel(eventId);

  Event.findById(eventId, handleEventErrors(null, null, event => {
    const participants = event.matching.participants.map(email => event.guests[email]);
    const allMatches = event.matching.matches;
    const allRoundsMatchings = [];

    allMatches.forEach(roundMatches => {
      Object.keys(roundMatches).forEach(firstGuestEmail => {
        const secondGuestEmail = roundMatches[firstGuestEmail].match;

        if ( roundMatches[firstGuestEmail].meet && roundMatches[secondGuestEmail].meet ) {
          const firstGuest = _.find(participants, { email: firstGuestEmail });
          const secondGuest = _.find(participants, { email: secondGuestEmail });

          if ( firstGuest && secondGuest ) {
            allRoundsMatchings.push([ firstGuest.id, secondGuest.id ]);
          }
        }
      });
    });

    runMatching(participants, allRoundsMatchings, eventId)
      .then(guestsPairs => {
        const matches = {};
        guestsPairs.forEach(pair => {
          const firstGuest = _.find(participants, { id: pair[0] });
          const secondGuest = _.find(participants, { id: pair[1] });
          matches[firstGuest.email] = { match: secondGuest.email, meet: false };
          matches[secondGuest.email] = { match: firstGuest.email, meet: false };
        });

        event.matching.matches.push(matches);

        Event.findByIdAndUpdate(eventId, {
          $set: {
            matching: {
              ...event.matching,
            }
          }
        }, () => {
          step(eventId, 'matching');
        });
      });
  }));
}

const step = (eventId, step) => {
  const eventData = getEventData(eventId);
  eventData.currentStep = step;

  eventData.nsp && eventData.nsp.to('playing').emit('changePage', step);
  eventData.nsp && eventData.nsp.emit('changeStep', step);
  Event.findById(eventId, handleEventErrors(null, null, event => {
    countDown(eventId, event.stepsDuration[step], handleTimeChange(eventId, step))
      .then(() => {
        goToNextStep(eventId, step);
      });
  }));
}

const endRound = eventId => {
  const eventData = getEventData(eventId);
  Event.findById(eventId, handleEventErrors(null, null, event => {
    const now = (new Date()).getTime();
    if ( getAdjustedEndDate(event) < now ) {
      eventData.currentStep = 'endEvent';
      eventData.nsp && eventData.nsp.emit('changeStep', 'endEvent');
      eventData.nsp && eventData.nsp.to('playing').to('waiting').emit('changePage', 'endEvent');
      const { sendRatingEmail } = require('./agenda');
      sendRatingEmail(eventId);
    } else {
      Event.findByIdAndUpdate(eventId, {
        $set: {
          matching: {
            ...event.matching,
            currentRound: event.matching.currentRound + 1,
          },
        },
      }, handleError(null, () => {
        startRound(eventId);
      }));
    }
  }));
}

const goToNextStep = (eventId, currentStep) => {
  if ( nextStep[currentStep] === 'startRound' ) {
    startRound(eventId);
  } else if ( nextStep[currentStep] !== null ) {
    step(eventId, nextStep[currentStep]);
  } else {
    endRound(eventId);
  }
}

const skipCurrentStep = eventId => {
  const eventData = getEventData(eventId);
  if ( eventData.started && eventData.currentStep !== 'endEvent' ) {
    clearEventInterval(eventId);
    goToNextStep(eventId, eventData.currentStep);
  }
}

const skipCurrentRound = eventId => {
  const eventData = getEventData(eventId);
  if ( eventData.started && eventData.currentStep !== 'endEvent' ) {
    clearEventInterval(eventId);
    endRound(eventId);
  }
}

const pauseEvent = eventId => {
  clearEventInterval(eventId);
}

const resumeEvent = eventId => {
  const eventData = getEventData(eventId);
  if ( eventData.started && eventData.currentStep !== 'endEvent' ) {
    if ( !eventData.interval ) {
      countDown(eventId, eventData.remainingTime, handleTimeChange(eventId, eventData.currentStep))
        .then(() => {
          goToNextStep(eventId, eventData.currentStep);
        });
    }
  }
}

const moveToWaitingChannel = (email, eventId) => {
  console.log('moveToWaitingChannel', email);
  const eventData = getEventData(eventId);
  const socket = eventData.sockets[email];

  eventsData[eventId].waiting.push(socket);

  socket.join('waiting');
  socket.leave('playing');

  socket.emit('changePage', 'waiting');
}

const removeAllFromWaitingChannel = eventId => {
  const eventData = getEventData(eventId);
  eventData.waiting.forEach(socket => {
    socket.leave('waiting');
    socket.join('playing');
  });

  eventsData[eventId].waiting = [];
}

const addSocket = (eventId, email, socket) => {
  const eventData = getEventData(eventId);
  eventData.sockets[email] = socket;

  if ( !eventData.nsp ) {
    eventData.nsp = socket.nsp;
  }

  socket.join('playing');
  socket.email = email;
}

const removeSocket = (eventId, email) => {
  const eventData = getEventData(eventId);
  eventData.sockets[email] = undefined;
}

const getCurrentStep = (eventId, socket) => {
  const eventData = getEventData(eventId);

  if ( !eventData.nsp ) {
    eventData.nsp = socket.nsp;
  }

  return eventData.currentStep;
}

module.exports = {
  addSocket,
  getAdjustedEndDate,
  getCurrentStep,
  hasStarted,
  moveToWaitingChannel,
  pauseEvent,
  removeSocket,
  resumeEvent,
  skipCurrentRound,
  skipCurrentStep,
  startEvent,
}