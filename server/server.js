const fs = require('fs');

const app = require('./app');
const { createSocket } = require('./controllers/socket');
const db = require('./db');

db.connect();

// const http = require('http').createServer(app);

const https = require('https').createServer({
  cert: fs.readFileSync('/etc/letsencrypt/live/api.intro-social.com/fullchain.pem'),
  key: fs.readFileSync('/etc/letsencrypt/live/api.intro-social.com/privkey.pem'),
}, app);

// const io = require('socket.io')(http);

const io = require('socket.io')(https);

createSocket(io);

const port = process.env.PORT || 3030;

// http.listen(port, function() {
https.listen(port, function() {
  console.log('Express server listening on port ' + port);
});
