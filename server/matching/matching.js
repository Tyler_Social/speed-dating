const { execFile } = require('child_process');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');

const bgl4Path = path.resolve(__dirname, './bgl4');
const participantsFilePrefix = 'participants-';
const matchingFilePrefix = 'matching-'
const genderMapping = {
  'Male': 1,
  'Female': 2,
  'Third Gender / Non-binary / Fluid': 3,
  'Other / Prefer not to answer': 3,
  'Non-binary / Fluid': 3,
  'Third Gender / Trans / Other': 3,
};

const runBGL4 = (eventId, participants) => new Promise((resolve, reject) => {
  const participantsFile = path.resolve(__dirname, `./${participantsFilePrefix}${eventId}`);
  const matchingFile = path.resolve(__dirname, `./${matchingFilePrefix}${eventId}`);
  const child = execFile(bgl4Path, [ participantsFile, matchingFile ], (error, stdout, stderr) => {
    if ( error ) {
      reject(error);
      return;
    }

    const lines = stdout.split('\n').filter(line => line !== '');
    const matches = [];

    lines.forEach(line => {
      const indexes = line.split(',');
      matches.push([participants[indexes[0]].id, participants[indexes[1]].id]);
    });

    resolve(matches);
  });
});

const writeFileCallback = (resolve, reject) => err => {
  if (err) {
    reject(err);
    return
  }

  resolve();
};

const generateParticipantsFile = (participants, eventId) => new Promise((resolve, reject) => {
  const filePath = path.resolve(__dirname, `./${participantsFilePrefix}${eventId}`);
  let fileContent = 'id, self, seek_1, seek_2, seek_3\n';

  participants.forEach((participant, index) => {
    const { gender, matchGender } = participant;
    let line = `${index}, ${genderMapping[gender]}`;

    for ( let i = 0; i < 3; i++ ) {
      if ( matchGender[i] ) {
        line += `, ${genderMapping[matchGender[i]]}`;
      } else {
        line += ', -1';
      }
    }

    fileContent += `${line}\n`;
  });

  fs.writeFile(filePath, fileContent, writeFileCallback(resolve, reject));
});

const generateMatchingsFile = (allRoundsMatchings, eventId, participants) => new Promise((resolve, reject) => {
  const filePath = path.resolve(__dirname, `./${matchingFilePrefix}${eventId}`);
  let fileContent = '';

  allRoundsMatchings.forEach(matching => {
    const indexP1 = _.findIndex(participants, { id: matching[0] });
    const indexP2 = _.findIndex(participants, { id: matching[1] });
    if ( indexP1 !== -1 && indexP2 !== -1 ) {
      fileContent += `${indexP1},${indexP2}\n`;
    }
  });

  fs.writeFile(filePath, fileContent, writeFileCallback(resolve, reject));
});

const runMatching = (participants, allRoundsMatchings, eventId) => new Promise((resolve, reject) => {
  Promise.all([ generateParticipantsFile(participants, eventId), generateMatchingsFile(allRoundsMatchings, eventId, participants) ])
    .then(() => {
      runBGL4(eventId, participants)
        .then(matches => {
          resolve(matches)
        });
    });
});

const removeMatchingFiles = eventId => {
  const participantsFilePath = path.resolve(__dirname, `./${participantsFilePrefix}${eventId}`);
  const matchingFilePath = path.resolve(__dirname, `./${matchingFilePrefix}${eventId}`);
  fs.unlink(participantsFilePath, (err) => {
    if (err) {
      console.log('Error removing: ' + participantsFilePath);
      console.log(err);
    }
  });
  fs.unlink(matchingFilePath, (err) => {
    if (err) {
      console.log('Error removing: ' + matchingFilePath);
      console.log(err);
    }
  });
}

module.exports = {
  runMatching,
  removeMatchingFiles,
};
