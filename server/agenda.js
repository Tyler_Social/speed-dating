const Agenda = require('agenda');
const _ = require('lodash');

const { returnCodeToCodeList } = require('./models/code');
const db = require('./db');
const { sendHostCode, sendRatingEmail } = require('./email');
const { startEvent } = require('./eventManager');
const { removeMatchingFiles } = require('./matching/matching');
const { handleError } = require('./utils');
const { getEventsData } = require('./wp');

const Event = require('./models/event');

const agenda = new Agenda();

agenda
  .database(db.getConnectionURL(), 'agendaJobs')
  .processEvery('5 seconds');

agenda.define('sendRatingEmail', job => {
  const { eventId } = job.attrs.data;
  sendRatingEmail(eventId);
});

const guestCodesAmount = 1000;
const maxHostCode = 1000000;

const createOrUpdateEvent = eventData => (new Promise((resolve, reject) => {
  const now = new Date();
  Event.findOne({ mysqlId: eventData.mysqlId }, handleError(null, event => {
    if ( !event ) {
      eventData.guest = {};

      eventData.guestCodes = _.shuffle(
        Array(guestCodesAmount).fill(0).map((v, i) => i.toString())
      );

      eventData.hostCode = Math.floor(Math.random() * maxHostCode);
      eventData.matching = {
        currentRound: 0,
        participants: [],
        matches: [],
      };

      const newEvent = new Event( eventData );
      newEvent.save(handleError(null, createdEvent => {
        console.log('Event created. Mysql id: ' + eventData.mysqlId + '. Name: ' + eventData.name + '. Code: ' + createdEvent.code + '. Host code: ' + createdEvent.hostCode);
        sendHostCode(createdEvent._id);
        agenda.schedule(
          new Date(eventData.startDate.getTime() - (60 * 60 * 1000)),
          'sendHostCode',
          { eventId: createdEvent._id }
        );
        agenda.schedule(
          new Date(eventData.endDate.getTime() + (24 * 60 * 60 * 1000)),
          'removeMatchingFiles',
          { eventId: createdEvent._id }
        );
        agenda.schedule(
          new Date(eventData.endDate.getTime() + (24 * 60 * 60 * 1000)),
          'returnEventCodeToCodeList',
          { code: createdEvent.code }
        );
        resolve();
      }));
    } else if ( eventData.startDate > now ) {
      eventData.matching = event.matching;
      Event.findByIdAndUpdate(event._id, { $set: eventData }, handleError(null, () => {
        console.log('Event updated. Mysql id: ' + eventData.mysqlId + '. Name: ' + eventData.name + '. Code: ' + event.code + '. Host code: ' + event.hostCode);
        resolve();
      }));
    } else {
      console.log('Event not updated, it has already started. Mysql id: ' + eventData.mysqlId + '. Name: ' + eventData.name + '. Code: ' + event.code + '. Host code: ' + event.hostCode);
      resolve();
    }
  }));
}));

const loopThroughEventsDataAndCreateOrUpdateEvent = (i, eventsData) => {
  if (i < eventsData.length ) {
    createOrUpdateEvent(eventsData[i]).then(() => {
      loopThroughEventsDataAndCreateOrUpdateEvent(i+1, eventsData);
    });
  }
}

agenda.define('getEvents', job => {
  getEventsData().then(eventsData => {
    if (eventsData) {
      eventsData = Object.keys(eventsData).map(mysqlId => {
        const eventData = eventsData[mysqlId];
        eventData.mysqlId = mysqlId;

        eventData.stepsDuration = {};
        [ 'matching', 'chat', 'rateMatch' ].forEach(durationKey => {
          const convertedDuration = parseInt(eventData[durationKey].split(' ')[0], 10) * 60;
          eventData.stepsDuration[durationKey] = !isNaN(convertedDuration) ? convertedDuration : undefined;
          eventData[durationKey] = undefined;
        });

        const validMeetType = [
          'Heterosexual Dating',
          'Homosexual Dating',
          'Non-Binary / Bisexual / Pansexual Dating',
        ].includes(eventData.meetType);

        if ( !validMeetType ) {
          eventData.meetType = undefined;
        }

        return eventData
      });

      loopThroughEventsDataAndCreateOrUpdateEvent(0, eventsData);
    }
  });
});

agenda.define('startEvent', job => {
  const { eventId } = job.attrs.data;
  startEvent(eventId);
});

const cancelEventStart = eventId => (new Promise((resolve, reject) => {
  agenda.cancel({ name: 'startEvent', data: { eventId: eventId } }).then(() => {
    resolve();
  });
}));

agenda.define('sendHostCode', job => {
  const { eventId } = job.attrs.data;
  sendHostCode(eventId);
});

agenda.define('removeMatchingFiles', job => {
  const { eventId } = job.attrs.data;
  removeMatchingFiles(eventId);
});

agenda.define('returnEventCodeToCodeList', job => {
  const { code } = job.attrs.data;
  returnCodeToCodeList(code);
});

module.exports = {
  cancelEventStart,
  sendRatingEmail: async eventId => {
    await agenda.start();
    await agenda.schedule('in 3 hours', 'sendRatingEmail', { eventId });
    // await agenda.schedule('in 5 minutes', 'sendRatingEmail', { eventId });
  },
  startGettingEvents: async () => {
    await agenda.start();
    // await agenda.every('30 minutes', 'getEvents');
    await agenda.every('30 seconds', 'getEvents');
  },
}
