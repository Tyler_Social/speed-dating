import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'

import initialState from './initialState'
import reducers from './reducers'
import sagas from './sagas'

import './index.css'
import App from './Components/App'
import * as serviceWorker from './serviceWorker';

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  reducers,
  { ...initialState, },
  applyMiddleware(sagaMiddleware),
)

sagaMiddleware.run(sagas)

const reactAppData = window.isAsdReactPlugin || { appSelector: '#react-app' }
const { appSelector } = reactAppData
const appAnchorElement = document.querySelector(appSelector)

if (appAnchorElement) {
  render(
    <React.StrictMode>
      <Provider store={ store }>
          <App />
      </Provider>
    </React.StrictMode>,
    appAnchorElement
  )
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
