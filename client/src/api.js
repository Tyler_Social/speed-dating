let baseURL = 'http://' + window.location.hostname + ':3030/api/'
// let baseURL = 'https://' + window.location.hostname + ':3030/api/'
// let baseURL = 'https://ec2-3-133-103-39.us-east-2.compute.amazonaws.com/api/'


if (process.env.NODE_ENV === 'production') {
  baseURL = 'https://api.intro-social.com/api/'
}

export const nextMatch = async (email, eventCode) => {
  const url = new URL(baseURL + 'event/nextMatch')
  url.search = new URLSearchParams({ email, eventCode })

  return await (await fetch(url)).json()
}

export const rateMatch = async (email, match, matchCode, eventCode) => {
  const url = new URL(baseURL + 'event/rateMatch')
  url.search = new URLSearchParams({ email, match, matchCode, eventCode })

  return await (await fetch(url)).json()
}

export const rateEvent = async (token, comment, rating) => {
  const url = new URL(baseURL + 'event/rateEvent/' + token)
  url.search = new URLSearchParams({ comment, rating })

  return await (await fetch(url)).json()
}

export const nextRound = async (eventCode, hostCode) => {
  const url = new URL(baseURL + 'host/nextRound')

  return await (await fetch(url, {
    body: JSON.stringify({
      eventCode, hostCode,
    }),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  })).json()
}

export const nextStep = async (eventCode, hostCode) => {
  const url = new URL(baseURL + 'host/nextStep')

  return await (await fetch(url, {
    body: JSON.stringify({
      eventCode, hostCode,
    }),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  })).json()
}

export const pauseEvent = async (eventCode, hostCode) => {
  const url = new URL(baseURL + 'host/pauseEvent')

  return await (await fetch(url, {
    body: JSON.stringify({
      eventCode, hostCode
    }),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  })).json()
}

export const resumeEvent = async (eventCode, hostCode) => {
  const url = new URL(baseURL + 'host/resumeEvent')

  return await (await fetch(url, {
    body: JSON.stringify({
      eventCode, hostCode,
    }),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  })).json()
}

export const startEvent = async (eventCode, hostCode) => {
  const url = new URL(baseURL + 'host/startEvent')

  return await (await fetch(url, {
    body: JSON.stringify({
      eventCode, hostCode,
    }),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  })).json()
}

export const updateStepsDuration = async (eventCode, hostCode, steps) => {
  const url = new URL(baseURL + 'host/updateStepsDuration')

  return await (await fetch(url, {
    body: JSON.stringify({
      eventCode, hostCode,
      steps,
    }),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  })).json()
}
