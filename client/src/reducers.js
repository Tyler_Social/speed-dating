import { handleActions } from 'redux-actions'

import {
    fillInput,
    nextMatchReceived,
    nextMatchRequest,
    setComment,
    setCurrentStep,
    setEventName,
    setGuests,
    setGuestCode,
    setMessage,
    setPage,
    setRating,
    setTimer,
    toggleGuest,
} from './actions'

import initialState from './initialState'

export default handleActions({
    [fillInput]: ( state, action ) => {
        const { field, form, value } = action.payload

        return {
            ...state,
            forms: {
                ...state.forms,
                [form]: {
                    ...state.forms[form],
                    [field]: value,
                }
            }
        }
    },
    [nextMatchReceived]: ( state, action ) => ({
        ...state,
        match: action.payload,
    }),
    [nextMatchRequest]: ( state, action ) => ({
        ...state,
        match: null,
    }),
    [setComment]: ( state, action ) => ({
        ...state,
        comment: action.payload,
    }),
    [setCurrentStep]: ( state, action ) => ({
        ...state,
        currentStep: action.payload,
    }),
    [setEventName]: ( state, action ) => ({
        ...state,
        eventName: action.payload,
    }),
    [setGuests]: ( state, action ) => ({
        ...state,
        guests: action.payload,
    }),
    [setGuestCode]: ( state, action ) => ({
        ...state,
        guestCode: action.payload,
    }),
    [setMessage]: ( state, action ) => ({
        ...state,
        message: action.payload,
    }),
    [setPage]: ( state, action ) => ({
        ...state,
        page: action.payload,
    }),
    [setRating]: ( state, action ) => ({
        ...state,
        rating: action.payload,
    }),
    [setTimer]: ( state, action ) => ({
        ...state,
        timer: action.payload,
    }),
    [toggleGuest]: ( state, action ) => ({
        ...state,
        expandedGuestIndex: state.expandedGuestIndex === action.payload ? -1 : action.payload,
    }),
}, initialState)
