import { eventChannel } from 'redux-saga'
import socketIO from 'socket.io-client'

import {
  fillInput,
  nextMatchRequest,
  setCurrentStep,
  setEventName,
  setGuestCode,
  setGuests,
  setMessage,
  setPage,
  setTimer
} from './actions'
import { toTimeString } from './utils'

let endPoint = 'http://' + window.location.hostname + ':3030/'
// let endPoint = 'https://' + window.location.hostname + ':3030/'
// let endPoint = 'https://ec2-3-133-103-39.us-east-2.compute.amazonaws.com/'

if (process.env.NODE_ENV === 'production') {
  endPoint = 'https://api.intro-social.com/'
}

export const connect = function (email, eventCode) {
  const socket = socketIO(endPoint + eventCode, {
    query: { email },
    reconnection: false,
  })

  return eventChannel(emit => {
    socket.on('changePage', page => {
      if ( page !== 'chat' ) {
        emit(setPage(page))
      }
    })

    socket.on('changeStep', step => {
      emit(setCurrentStep(step))

      if ( step === 'matching' && !/^\d{1,6}$/.test(email) ) {
        emit(nextMatchRequest({ email, eventCode }))
      }
    })

    socket.on('message', message => {
      emit(setMessage(message))
    })

    socket.on('timer', timer => {
      emit(setTimer( timer ))
    })

    socket.on('eventData', data => {
      if ( data.eventName ) {
        emit(setEventName(data.eventName))
      }

      if ( data.guestCode ) {
        emit(setGuestCode(data.guestCode))
      }

      if ( data.steps ) {
        [ 'chat', 'matching', 'rateMatch' ].forEach(step => {
          emit(fillInput({
            form: 'durations',
            field: step,
            value: toTimeString(data.steps[step]),
          }))
        })
      }

      if ( data.guests ) {
        emit(setGuests(data.guests))
      }
    })

    return () => {
      socket.disconnect()
    }
  })
}
