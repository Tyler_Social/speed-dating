import { createAction } from 'redux-actions'

export const connect = createAction('CONNECT')

export const createSocket = createAction('CREATE_SOCKET')

export const fillInput = createAction('FILL_INPUT')

export const nextMatchReceived = createAction('NEXT_MATCH_RECEIVED')
export const nextMatchRequest = createAction('NEXT_MATCH_REQUEST')

export const nextRoundRequest = createAction('NEXT_ROUND_REQUEST')

export const nextStepRequest = createAction('NEXT_STEP_REQUEST')

export const pauseEventRequest = createAction('PAUSE_EVENT_REQUEST')

export const rateEventRequest = createAction('RATE_EVENT_REQUEST')

export const rateMatchRequest = createAction('RATE_MATCH_REQUEST')

export const resumeEventRequest = createAction('RESUME_EVENT_REQUEST')

export const setComment = createAction('SET_COMMENT')

export const setCurrentStep = createAction('SET_CURRENT_STEP')

export const setEventName = createAction('SET_EVENT_NAME')

export const setGuests = createAction('SET_GUESTS')

export const setGuestCode = createAction('SET_GUEST_CODE')

export const setMessage = createAction('SET_MESSAGE')

export const setPage = createAction('SET_PAGE')

export const setRating = createAction('SET_RATING')

export const setTimer = createAction('SET_TIMER')

export const startEventRequest = createAction('START_EVENT_REQUEST')

export const toggleGuest = createAction('TOGGLE_GUEST')

export const updateStepsDurationRequest = createAction('UPDATE_STEPS_DURATION_REQUEST')
