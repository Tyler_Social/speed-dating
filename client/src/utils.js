export const toTimeString = time => (
  Math.floor(time / 60) + ':' + (Math.floor(time % 60) < 10 ? '0' : '') + Math.floor(time % 60)
)
