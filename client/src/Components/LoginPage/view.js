import React from 'react'

import './style.css'

const LoginPage = ({ createSocket, fillCode, email, eventCode }) => (
  <div className='login-page'>
    <h2>Login</h2>
    <div className='login-page-form'>
      <label>Enter event code</label>
      <input onChange={ e => { fillCode('eventCode', e.target.value) } } type='text' value={ eventCode } />
    </div>
    <div className='login-page-form'>
      <label>Enter email address</label>
      <input onChange={ e => { fillCode('email', e.target.value) } } type='text' value={ email } />
    </div>
    <button onClick={() => { createSocket(email, eventCode) }}>Ok</button>
  </div>
)

export default LoginPage
