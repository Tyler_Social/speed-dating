import { connect } from 'react-redux'

import { createSocket, fillInput } from '../../actions'

import LoginPage from './view'

const mapStateToProps = state => ({
  email: state.forms.login.email,
  eventCode: state.forms.login.eventCode,
})

const mapDispatchToProps = dispacth => ({
  fillCode: ( field, value ) => {
    dispacth(fillInput({
      form: 'login',
      field,
      value
    }))
  },
  createSocket: ( email, eventCode ) => {
    dispacth(createSocket({ email, eventCode }))
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)
