import React from 'react'

import ChatPage from '../ChatPage'
import EventEndPage from '../EventEndPage'
import GuestCode from '../GuestCode'
import HostPage from '../HostPage'
import LoginPage from '../LoginPage'
import MatchingPage from '../MatchingPage'
import Message from '../Message'
import RateEventPage from '../RateEventPage'
import RateMatchPage from '../RateMatchPage'
import WaitingPage from '../WaitingPage'

import './style.css'

const App = ({ email, eventCode, hasMessage, page }) => {
  const urlParams = new URLSearchParams(window.location.search);
  const token = urlParams.get('token');

  let pageComponent = <LoginPage />

  if ( token !== null ) {
    pageComponent = <RateEventPage token={ token } />
  } else if ( email && eventCode ) {
    switch ( page ) {
      case 'chat':
        pageComponent = <ChatPage />
        break
      case 'endEvent':
        pageComponent = <EventEndPage />
        break
      case 'host':
        pageComponent = <HostPage />
        break
      case 'login':
        pageComponent = <LoginPage />
        break
      case 'matching':
        pageComponent = <MatchingPage />
        break
      case 'rateMatch':
        pageComponent = <RateMatchPage />
        break
      case 'waiting':
        pageComponent = <WaitingPage />
        break
      default:
        pageComponent = <LoginPage />
        break
    }
  }

  return (
    <div className={'app' + (hasMessage ? ' has-message' : '')}>
      <Message />
      <GuestCode />
      { pageComponent }
    </div>
  )
}

export default App
