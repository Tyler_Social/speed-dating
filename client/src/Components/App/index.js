import { connect } from 'react-redux'

import App from './view'

const mapStateToProps = state => ({
  email: state.forms.login.email,
  eventCode: state.forms.login.eventCode,
  hasMessage: state.message !== null,
  page: state.page,
})

export default connect(mapStateToProps)(App)
