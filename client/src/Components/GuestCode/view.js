import React from 'react'

import './style.css'

const GuestCode = ({ guestCode }) => {
  if ( guestCode ) {
    return (
      <div className='guest-code'>This is your code: <strong>{ guestCode }</strong></div>
    )
  }

  return null
}

export default GuestCode
