import { connect } from 'react-redux'

import GuestCode from './view.js'

const mapStateToProps = state => ({
  guestCode: state.guestCode,
})

export default connect(mapStateToProps)(GuestCode)
