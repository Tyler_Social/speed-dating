import React from 'react'

import './style.css'

const RateEventPage = ({ comment, rating, rateEvent, setComment, setRating, token }) => (
  <div className='rate-event-page'>
    <p>Thank you for attending the event!</p>
    <p>Please rate your experience to receive your results.</p>
    <div className='stars'>
      <div
        className={ 'star-wrapper' + ( rating === 5 ? ' selected' : '' ) }
        onClick={ () => { setRating(5) } }
      >
        <div className='empty-star'>☆</div>
        <div className='full-star'>★</div>
      </div>
      <div
        className={ 'star-wrapper' + ( rating === 4 ? ' selected' : '' ) }
        onClick={ () => { setRating(4) } }
      >
        <div className='empty-star'>☆</div>
        <div className='full-star'>★</div>
      </div>
      <div
        className={ 'star-wrapper' + ( rating === 3 ? ' selected' : '' ) }
        onClick={ () => { setRating(3) } }
      >
        <div className='empty-star'>☆</div>
        <div className='full-star'>★</div>
      </div>
      <div
        className={ 'star-wrapper' + ( rating === 2 ? ' selected' : '' ) }
        onClick={ () => { setRating(2) } }
      >
        <div className='empty-star'>☆</div>
        <div className='full-star'>★</div>
      </div>
      <div
        className={ 'star-wrapper' + ( rating === 1 ? ' selected' : '' ) }
        onClick={ () => { setRating(1) } }
      >
        <div className='empty-star'>☆</div>
        <div className='full-star'>★</div>
      </div>
    </div>
    <p>How could we improve? (optional)</p>
    <div><input onChange={ e => { setComment(e.target.value) } } type='text' value={ comment } /></div>
    <div><button onClick={ () => { rateEvent(token, comment, rating) } }>Submit</button></div>
  </div>
)
export default RateEventPage
