import { connect } from 'react-redux'

import { rateEventRequest, setComment, setRating } from '../../actions'

import RateEventPage from './view'

const mapStateToProps = state => ({
  comment: state.comment,
  rating: state.rating,
})

const mapDispatchToProps = dispacth => ({
  rateEvent: ( token, comment, rating ) => {
    dispacth(rateEventRequest({ token, comment, rating }))
  },
  setComment: comment => {
    dispacth(setComment(comment))
  },
  setRating: rating => {
    dispacth(setRating(rating))
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(RateEventPage)
