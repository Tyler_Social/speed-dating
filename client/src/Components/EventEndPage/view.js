import React from 'react'

import './style.css'

const EventEndPage = () => (
  <div className='event-end-page'>
    <h3>The event is over!</h3>
    <p>The room is now closed. Thank you for attending!</p>
    <p className='small'>Your match results will be emailed to you in 3 hours.</p>
  </div>
)
export default EventEndPage
