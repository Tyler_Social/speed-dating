import { connect } from 'react-redux'

import { setPage } from '../../actions'
import MatchingPage from './view'

const mapStateToProps = state => ({
  match: state.match,
  timer: state.timer,
})

const mapDispatchToProps = dispatch => ({
  setPage: page => {
    dispatch(setPage(page))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(MatchingPage)
