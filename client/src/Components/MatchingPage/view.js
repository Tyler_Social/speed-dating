import React from 'react'

import { toTimeString } from '../../utils'

import './style.css'

const MatchingPage = ({ match, setPage, timer }) => {
  let matchData
  if (match) {
    matchData = (
      <div>
        <div className='row'>
          <div><img alt='match profile' src={ match.profilePath }/></div>
          <div className='timer'>
            <h2>{ toTimeString(timer) }</h2>
          </div>
        </div>
        <div className='small'>{ match.description }</div>
        <div className='person-id'>{ match.code }</div>
      </div>
    )
  } else {
    matchData = (
      <div>
        Loading match...
      </div>
    )
  }

  return (
    <div className='matching-page'>
      <p>Time to start the show!</p>
      <p>You're looking for this person:</p>
      { matchData }
      <p className='small'>When you find your next match, click to "go" button below</p>
      <div className='row'>
        <button onClick={ () => { setPage('chat') } }>Go!</button>
        <button onClick={ () => { setPage('waiting') } }>Skip this round</button>
      </div>
    </div>
  )
}

export default MatchingPage
