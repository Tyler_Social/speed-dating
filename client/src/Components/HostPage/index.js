import { connect } from 'react-redux'

import {
  fillInput,
  nextStepRequest,
  nextRoundRequest,
  pauseEventRequest,
  resumeEventRequest,
  startEventRequest,
  toggleGuest,
  updateStepsDurationRequest,
} from '../../actions'
import HostPage from './view.js'

const mapStateToProps = state => ({
  chat: state.forms.durations.chat,
  currentStep: state.currentStep,
  email: state.forms.login.email,
  eventCode: state.forms.login.eventCode,
  eventName: state.eventName,
  expandedGuestIndex: state.expandedGuestIndex,
  guests: state.guests,
  matching: state.forms.durations.matching,
  rateMatch: state.forms.durations.rateMatch,
  timer: state.timer,
})

const mapDispatchToProps = dispatch => ({
  fillInput: (field, value) => {
    if ( /^(\d{1,2}(:(\d{1,2})?)?)?$/.test(value) ) {
      dispatch(fillInput({
        form: 'durations',
        field,
        value,
      }))
    }
  },
  nextStep: (eventCode, hostCode) => {
    dispatch(nextStepRequest({ eventCode, hostCode }))
  },
  nextRound: (eventCode, hostCode) => {
    dispatch(nextRoundRequest({ eventCode, hostCode }))
  },
  pauseEvent: (eventCode, hostCode) => {
    dispatch(pauseEventRequest({ eventCode, hostCode }))
  },
  resumeEvent: (eventCode, hostCode) => {
    dispatch(resumeEventRequest({ eventCode, hostCode }))
  },
  startEvent: (eventCode, hostCode) => {
    dispatch(startEventRequest({ eventCode, hostCode }))
  },
  toggleGuest: index => {
    dispatch(toggleGuest(index))
  },
  updateStepsDuration: (eventCode, hostCode, matching, chat, rateMatch) => {
    dispatch(updateStepsDurationRequest({
      eventCode,
      hostCode,
      steps: {
        matching: parseInt(matching.split(':')[1], 10) + (parseInt(matching.split(':')[0], 10) * 60),
        chat: parseInt(chat.split(':')[1], 10) + (parseInt(chat.split(':')[0], 10) * 60),
        rateMatch: parseInt(rateMatch.split(':')[1], 10) + (parseInt(rateMatch.split(':')[0], 10) * 60),
      },
    }))
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(HostPage)
