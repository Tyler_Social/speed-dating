import React from 'react'
import moment from 'moment'
import _ from 'lodash'

import { toTimeString } from '../../utils'

import './style.css'

const HostPage = ({
  chat,
  currentStep,
  email,
  eventCode,
  eventName,
  expandedGuestIndex,
  guests,
  matching,
  rateMatch,
  timer,
  fillInput,
  nextRound,
  nextStep,
  pauseEvent,
  resumeEvent,
  startEvent,
  toggleGuest,
  updateStepsDuration
}) => {
  return (
    <div className='host-page'>
      <h1>{ eventName } - { eventCode }</h1>
      <div className='actions'>
        <div className='controls'>
          <div><strong>Current Mode: { _.capitalize(currentStep) }</strong><span>{ toTimeString(timer) } ( min : sec )</span></div>
          <button onClick={ () => { startEvent(eventCode, email) } }>Start event</button>
          <p>Do not start the meeting until at least a few guests have logged-in to live event.</p>
          <button onClick={ () => { pauseEvent(eventCode, email) } }>Pause</button>
          <button onClick={ () => { resumeEvent(eventCode, email) } }>Resume</button>
          <button onClick={ () => { nextStep(eventCode, email) } }>Next Step</button>
          <button onClick={ () => { nextRound(eventCode, email) } }>Skip to Next Round</button>
        </div>
        <div className='durations'>
          <div className='input-group'>
            <label>Mode</label>
            <label>Duration ( min : sec )</label>
          </div>
          <div className='input-group'>
            <label>Matching</label>
            <input type='text' value={ matching } onChange={e => { fillInput('matching', e.target.value) }}/></div>
          <div className='input-group'>
            <label>Chating</label>
            <input type='text' value={ chat } onChange={e => { fillInput('chat', e.target.value) }}/></div>
          <div className='input-group'>
            <label>Rating</label>
            <input type='text' value={ rateMatch } onChange={e => { fillInput('rateMatch', e.target.value) }}/></div>
          <button onClick={ () => { updateStepsDuration(eventCode, email, matching, chat, rateMatch) } }>Update</button>
        </div>
      </div>
      <div className='guest-list'>
        { guests.map((guest, index) => (
          <div key={ guest.code }>
            <div className='header' onClick={ () => { toggleGuest(index) } }>
              { guest.nickname } - { guest.code }<i className={'fas fa-caret-' + ( expandedGuestIndex === index ? 'up' : 'down' )}></i>
            </div>
            <div className={'body' + ( expandedGuestIndex === index ? ' open' : '' )}>
              <div>
                <div className='profile-image' style={ { backgroundImage: 'url(\'https://place-hold.it/100x100\')' } }></div>
                <div>
                  <div><label>Gender:</label> { guest.gender }</div>
                  <div><label>Date of birth:</label> { moment(guest.dateOfBirth).format('MM/DD/YYYY') }</div>
                </div>
              </div>
              <div><label>Description:</label> { guest.description }</div>
              <div><label>Matches gender:</label> { guest.matchGender && guest.matchGender.join(', ') }</div>
              <div><label>Matches</label></div>
              <ul>
                { guest.matches ? Object.keys(guest.matches).map(matchKey => (
                  <li key={ matchKey }>{ matchKey }: { guest.matches[matchKey] ? 'Yes' : 'No' }</li>
                )) : null }
              </ul>
            </div>
          </div>
        )) }
      </div>
    </div>
  )
}

export default HostPage
