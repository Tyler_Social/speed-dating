import React from 'react'

import { toTimeString } from '../../utils'

import './style.css'

const ChatPage = ({ timer }) => (
  <div className='chat-page'>
    <p>Put your phone down and talk to your potential match</p>
    <div className='timer'>
      <h2>{ toTimeString(timer) }</h2>
    </div>
  </div>
)

export default ChatPage
