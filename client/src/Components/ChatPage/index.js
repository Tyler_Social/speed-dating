import { connect } from 'react-redux'

import ChatPage from './view'

const mapStateToProps = state => ({
  timer: state.timer,
})

export default connect(mapStateToProps)(ChatPage)
