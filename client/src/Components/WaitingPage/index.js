import { connect } from 'react-redux'

import WaitingPage from './view'

const mapStateToProps = state => ({
  currentStep: state.currentStep,
  event: state.eventName,
  timer: state.timer,
})

export default connect(mapStateToProps)(WaitingPage)
