import React from 'react'
import _ from 'lodash'

import { toTimeString } from '../../utils'

import './style.css'

const WaitingPage = ({ currentStep, event, timer }) => {
  let paragraph
  if ( currentStep === 'waiting' ) {
    paragraph = (
      <p>
        Welcome to <strong>{ event }</strong>!<br />
        You're all set! Please wait for the host to start the meeting rounds.
      </p>
    )
  } else {
    paragraph = (
      <p>
        Welcome to <strong>{ event }</strong>!<br />
        You're all set! Please, wait until next round starts.<br />
      </p>
    )
  }

  return (
    <div className='waiting-page'>
      { paragraph }
      <div className='timer'>
        <h2>{_.capitalize(currentStep)} { toTimeString(timer) }</h2>
      </div>
    </div>
  )
}

export default WaitingPage
