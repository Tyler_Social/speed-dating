import { connect } from 'react-redux'

import { rateMatchRequest } from '../../actions'

import RateMatchPage from './view'

const mapStateToProps = state => ({
  email: state.forms.login.email,
  matchCode: state.match ? state.match.code : '',
  eventCode: state.forms.login.eventCode,
  timer: state.timer,
})

const mapDispatchToProps = dispatch => ({
  rateMatch: (email, match, matchCode, eventCode) => {
    dispatch(rateMatchRequest({ email, match, matchCode, eventCode }))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(RateMatchPage)
