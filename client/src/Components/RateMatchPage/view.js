import React from 'react'

import { toTimeString } from '../../utils'

import './style.css'

const RateMatchPage = ({ email, matchCode, rateMatch, eventCode, timer }) => (
  <div className='rate-match-page'>
    <p>Tell us what you think!</p>
    <p>Do you want to stay in contact with { matchCode }</p>
    <div className='timer'>
      <h2>{ toTimeString(timer) }</h2>
    </div>
    <div className='row'>
      <button onClick={() => { rateMatch(email, true, matchCode, eventCode) }}>Yes</button>
      <button onClick={() => { rateMatch(email, false, matchCode, eventCode) }}>No</button>
      <button onClick={() => { rateMatch(email, null, matchCode, eventCode) }}>Did not meet</button>
    </div>
  </div>
)

export default RateMatchPage
