import { connect } from 'react-redux'

import { setMessage } from '../../actions'

import Message from './view'

const mapStateToProps = state => ({
  message: state.message,
})

const mapDispatchToProps = dispatch => ({
  clearMessage: () => {
    dispatch(setMessage(null))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Message)
