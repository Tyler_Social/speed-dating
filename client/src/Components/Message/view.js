import React, { useEffect } from 'react'

import './style.css'

const Message = ({ clearMessage, message }) => {
  useEffect(() => {
    if ( message !== null ) {
      const timeout = setTimeout(clearMessage, 10000)

      return () => {
        clearTimeout(timeout)
      }
    }
  }, [ message, clearMessage ])

  if ( message !== null ) {
    return (
      <div className='message has-content'>
        <div className='message-content'>{ message }</div>
        <div className='message-close-icon' onClick={ clearMessage }><i className='fas fa-times'></i></div>
      </div>
    )
  }

  return (
    <div className='message'></div>
  )
}

export default Message
