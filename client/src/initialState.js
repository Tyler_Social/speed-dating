export default {
  comment: '',
  currentStep: 'waiting',
  eventName: '',
  expandedGuestIndex: -1,
  forms: {
    durations: {
      chat: '',
      matching: '',
      rateMatch: '',
    },
    login: {
      email: '',
      eventCode: '',
    },
  },
  guestCode: null,
  guests: [],
  match: null,
  message: null,
  page: 'login',
  rating: 0,
  timer: 0,
}
