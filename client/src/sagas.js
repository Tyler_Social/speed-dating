import { call, put, takeEvery } from 'redux-saga/effects'

import {
  createSocket,
  nextMatchReceived,
  nextMatchRequest,
  nextRoundRequest,
  nextStepRequest,
  pauseEventRequest,
  rateEventRequest,
  rateMatchRequest,
  resumeEventRequest,
  setMessage,
  setPage,
  startEventRequest,
  updateStepsDurationRequest,
} from './actions'

import {
  nextMatch,
  nextRound,
  nextStep,
  pauseEvent,
  rateEvent,
  rateMatch,
  resumeEvent,
  startEvent,
  updateStepsDuration,
} from './api'

import { connect } from './socket'

const callNextMatch = function* ({ payload }) {
  const { email, eventCode } = payload
  const response = yield call(nextMatch, email, eventCode)
  if ( response.code === -1 ) {
    yield put(setMessage('You have no match for this round.'))
    yield put(setPage('waiting'))
  } else {
    yield put(nextMatchReceived(response))
  }
}

const callNextRound = function* ({ payload }) {
  const { eventCode, hostCode } = payload
  yield call(nextRound, eventCode, hostCode)
}

const callNextStep = function* ({ payload }) {
  const { eventCode, hostCode } = payload
  yield call(nextStep, eventCode, hostCode)
}

const callPauseEvent = function* ({ payload }) {
  const { eventCode, hostCode } = payload
  yield call(pauseEvent, eventCode, hostCode)
}

const callRateEvent = function* ({ payload }) {
  const { token, comment, rating } = payload
  const response = yield call(rateEvent, token, comment, rating)
  yield put(setMessage(response.message))
}

const callRateMatch = function* ({ payload }) {
  const { email, match, matchCode, eventCode } = payload
  const response = yield call(rateMatch, email, match, matchCode, eventCode)
  yield put(setMessage(response.message))
}

const callResumeEvent = function* ({ payload }) {
  const { eventCode, hostCode } = payload
  yield call(resumeEvent, eventCode, hostCode)
}

const callStartEventRequest = function* ({ payload }) {
  const { eventCode, hostCode } = payload
  yield call(startEvent, eventCode, hostCode)
}

const callUpdateStepsDuration = function* ({ payload }) {
  const { eventCode, hostCode, steps } = payload
  const response = yield call(updateStepsDuration, eventCode, hostCode, steps)
  yield put(setMessage(response.message))
}

const createSocketSaga = function* ({ payload }) {
  const { email, eventCode } = payload

  const socketChannel = connect(email, eventCode)

  yield takeEvery(socketChannel, function* ( action ) {
    yield put(action)
  })
}

export default function* rootSaga() {
  yield takeEvery(createSocket().type, createSocketSaga)
  yield takeEvery(nextMatchRequest().type, callNextMatch)
  yield takeEvery(nextRoundRequest().type, callNextRound)
  yield takeEvery(nextStepRequest().type, callNextStep)
  yield takeEvery(pauseEventRequest().type, callPauseEvent)
  yield takeEvery(rateEventRequest().type, callRateEvent)
  yield takeEvery(rateMatchRequest().type, callRateMatch)
  yield takeEvery(resumeEventRequest().type, callResumeEvent)
  yield takeEvery(startEventRequest().type, callStartEventRequest)
  yield takeEvery(updateStepsDurationRequest().type, callUpdateStepsDuration)
}